package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    ArrayList<Spell> spells;

    public ChainSpell(ArrayList<Spell> spells){
        this.spells = spells;
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }

    @Override
    public void undo(){
        while(!this.spells.isEmpty()){
            this.spells.get(this.spells.size()-1).cast();
            this.spells.remove(this.spells.size()-1);
        }
    }

    @Override
    public void cast(){
        for(Spell s: spells){
            s.cast();
        }
    }
}
